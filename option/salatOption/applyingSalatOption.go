package salatOption

type ApplyingSalatOption interface {
	Apply(o *SalatOption)
}
